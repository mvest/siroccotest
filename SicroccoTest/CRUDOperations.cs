﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace SicroccoTest
{
    class CRUDOperations
    {

        public static Guid CreateAccount(string name, Guid parent, IOrganizationService service)
        {
            //TODO add support for all attributes
            Entity account = new Entity("account");
            account["name"] = name;
            if (parent != Guid.Empty)
                account["parentaccountid"] = new EntityReference("account", parent);
            Console.WriteLine("Account created: {0}", name);
            return service.Create(account);
        }

        public static Guid CreateContact(string firstname, string lastname, IOrganizationService service)
        {
            //TODO add support for all attributes
            Entity contact = new Entity("contact");
            contact["firstname"] = firstname;
            contact["lastname"] = lastname;
            Console.WriteLine("Contact created: {0} {1}", firstname, lastname);
            return service.Create(contact);
        }

        public static Guid CreateNote(string noteText, string subject, EntityReference target, IOrganizationService service)
        {
            //TODO add support for all attributes
            Entity note = new Entity("annotation");
            note["subject"] = subject;
            note["notetext"] = noteText;
            note["objectid"] = target;
            Console.WriteLine("Note created: {0}, {1}", subject, noteText);
            return service.Create(note);
        }
        

        public static void Associate(EntityReference entity, EntityReference target, Relationship relationship, IOrganizationService service)
        {
            EntityReferenceCollection relatedEntities = new EntityReferenceCollection();
            relatedEntities.Add(entity);
            service.Associate(target.LogicalName, target.Id, relationship, relatedEntities);
            Console.WriteLine("{0} is now associated with {1}", entity.LogicalName , target.LogicalName);
        }

        
        public static void Update(string type, Guid ID, Dictionary<string, object> attributesToUpdate, IOrganizationService service)
        {

            Entity entityToUpdate = service.Retrieve(type, ID, new ColumnSet(true));
            foreach (var pair in attributesToUpdate)
            {
                entityToUpdate[pair.Key as string] = pair.Value;
            }

            service.Update(entityToUpdate);
            Console.WriteLine(type + " Has been updated");
        }

    }
}
