﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Xrm.Sdk.Query;
using Microsoft.Crm.Sdk.Messages;
using System.Net;
using System.ServiceModel.Description;

namespace SicroccoTest
{
    public class DynamicsConnection
    {
        //For a secure application this information should be encrpted.
        private static string _username = "magnus.ewerlof@pocalyps.com";
        private static string _password = "Sirocco1";
        private static string _remoteUri = "https://pocalyps0.api.crm4.dynamics.com/XRMServices/2011/Organization.svc";

       
        public static IOrganizationService Service()
        {
            IOrganizationService organizationService = null;
            try
            {
                ClientCredentials clientCredentials = new ClientCredentials();
                clientCredentials.UserName.UserName = _username;
                clientCredentials.UserName.Password = _password;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                
                organizationService = new OrganizationServiceProxy(new Uri(_remoteUri), null, clientCredentials, null);
              
               //Test the connection -- will throw if it fails.
                Guid userid = ((WhoAmIResponse)organizationService.Execute(new WhoAmIRequest())).UserId;
                Console.WriteLine("Connection success with userID: " + userid);


            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception caught - " + ex.Message + ex.InnerException);
                throw new Exception("Failed to establish Dynamics connection");
            }

            return organizationService;

        }
    }
}
