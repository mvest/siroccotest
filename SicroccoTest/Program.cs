﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using Newtonsoft.Json; 


namespace SicroccoTest
{
   
    class Program
    {

        static void Main(string[] args)
        {
            IOrganizationService service = null;

            try
            {

                service = DynamicsConnection.Service();

                //Create the first account
                Guid account1ID = CRUDOperations.CreateAccount("Account 1", Guid.Empty, service);

                //Create the second account as a child to the first account.
                Guid account2ID = CRUDOperations.CreateAccount("Account 2", account1ID, service);

                //Create the contacts
                Guid contact1ID = CRUDOperations.CreateContact("Kalle", "Karlsson", service);
                Guid contact2ID = CRUDOperations.CreateContact("Donald", "Trump", service);

                //Associate contacts to their accounts
                CRUDOperations.Associate(
                    new EntityReference("contact", contact1ID),
                    new EntityReference("account", account1ID),
                    new Relationship("contact_customer_accounts"),
                    service
                    );

                CRUDOperations.Associate(
                 new EntityReference("contact", contact2ID),
                 new EntityReference("account", account2ID),
                 new Relationship("contact_customer_accounts"),
                 service
                 );

                //Update account
                Dictionary<string, object> newAccountAttributes = new Dictionary<string, object>();
                newAccountAttributes.Add("name", "second account");
                newAccountAttributes.Add("primarycontactid", new EntityReference("contact", contact2ID));
                CRUDOperations.Update("account", account2ID, newAccountAttributes, service);
                

                //Update contact
                Dictionary<string, object> newContactAttributes = new Dictionary<string, object>();
                newContactAttributes.Add("firstname", "Carl");
                newContactAttributes.Add("lastname", "Carlsson");
                CRUDOperations.Update("contact", contact1ID, newContactAttributes, service);

                //create the notes
                Guid note1ID = CRUDOperations.CreateNote("Note 1 content", "Note 1", new EntityReference("account", account1ID), service);
                Guid note2ID = CRUDOperations.CreateNote("Note 2 content", "Note 2", new EntityReference("contact", contact2ID), service);
                Guid note3ID = CRUDOperations.CreateNote("Note 3 content", "Note 3", new EntityReference("contact", contact2ID), service);


                //The query gives access to all entities as requested. BUT, to access the notes of the second contact i had to use the primaryContactId of the second account. 
                //This would break if there is more then 1 contact with notes. 
                //I think it would be possible to get all entities with one query if i would use a filtered view and access the database directly with a regular SQL query. SELECT * FROM dbo.FilteredAccount
                List<string> notes = new List<string>();
                foreach (Entity entity in QueryAllEntities(service).Entities)
                {
                    //Uncomment line below to se the whole structure of each entity
                    //Dump(entity);

                    //Since there was no clear instruction of how to format the list i took the basic approach of making a string list of the notes only.

                    if (entity.Contains("accountAnnotation.notetext"))
                    {
                        string name = entity["name"] as string;
                        string noteText = (entity["accountAnnotation.notetext"] as AliasedValue).Value.ToString();
                        string listEntry = "Account: " + name + ", Notetext: " + noteText;
                        notes.Add(listEntry);
                    }

                    if(entity.Contains("contactAnnotaion.notetext"))
                    {
                        string firstname = (entity["contact.firstname"] as AliasedValue).Value.ToString();
                        string lastname = (entity["contact.lastname"] as AliasedValue).Value.ToString();
                        string noteText = (entity["contactAnnotaion.notetext"] as AliasedValue).Value.ToString();
                        string listEntry = "Contact: " + firstname + " " + lastname + ", Notetext: " + noteText;
                        notes.Add(listEntry);
                    }
               
                }

                Console.WriteLine("\nPrinting list with notes:");
                foreach (string note in notes)
                { 
                    Console.WriteLine(note);
                }

 
                Console.WriteLine("\nDone - Press any button to delete all entries");
                Console.ReadKey();
                Console.WriteLine("Performing delete...");
                service.Delete("annotation", note1ID);
                service.Delete("annotation", note2ID);
                service.Delete("annotation", note3ID);
                service.Delete("contact", contact1ID);
                service.Delete("contact", contact2ID);
                service.Delete("account", account1ID);
                service.Delete("account", account2ID);
                
                Console.WriteLine("All records deleted, Press any key to exit");


            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception caught - " + ex.Message + ex.InnerException);
            }
            Console.ReadKey();

        }


        private static EntityCollection QueryAllEntities(IOrganizationService service)
        {
            QueryExpression query = new QueryExpression("account");
            query.ColumnSet = new ColumnSet("name");

            LinkEntity contact = new LinkEntity("account", "contact", "accountid", "parentcustomerid", JoinOperator.LeftOuter);
            contact.Columns = new ColumnSet("firstname", "lastname");
            contact.EntityAlias = "contact";
            query.LinkEntities.Add(contact);

            LinkEntity accountAnnotation = new LinkEntity("account", "annotation", "accountid", "objectid", JoinOperator.LeftOuter);
            accountAnnotation.Columns = new ColumnSet("notetext");
            accountAnnotation.EntityAlias = "accountAnnotation";
            query.LinkEntities.Add(accountAnnotation);

            LinkEntity contactAnnotaion = new LinkEntity("account", "annotation", "primarycontactid", "objectid", JoinOperator.LeftOuter);
            contactAnnotaion.Columns = new ColumnSet("notetext");
            contactAnnotaion.EntityAlias = "contactAnnotaion";
            query.LinkEntities.Add(contactAnnotaion);


            var result = service.RetrieveMultiple(query);
            Console.WriteLine("\nThe query returned {0} entities", result.Entities.Count);
            return result;
        }

        private static void Dump(object o)
        {
            string json = JsonConvert.SerializeObject(o, Formatting.Indented);
            Console.WriteLine(json);
        }


    }
}
